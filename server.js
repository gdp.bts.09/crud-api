const express = require('express');
const users = require('./src/users/userRoutes');

// const{ success } = require('./response')

const app = express();
const port = 3000;

app.use(express.json());

app.use("/users", users);

app.listen(port, () => {
    console.log(`Example app listening on port http://localhost:${port}`)
})

app.get('/', (req, res) => {
    res.send('Hello World!')
})
